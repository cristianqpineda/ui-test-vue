import axios from "axios";

export const getCelebrities = () => axios("http://localhost:4000/data");
export const patchVote = (id, data) =>
  axios.patch(`http://localhost:4000/data/${id}`, data, {
    headers: { "Content-Type": "application/json" }
  });
