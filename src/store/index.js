import Vue from "vue";
import Vuex from "vuex";
import voting from "@/store/modules/voting";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    voting
  }
});
