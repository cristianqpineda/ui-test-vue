import { getCelebrities, patchVote } from "@/services";
import { getVotePercentages, processVote, updateCelebrities } from "@/utils";

export default {
  state: {
    celebrities: []
  },
  mutations: {
    SET_CELEBRITIES(state, celebrities) {
      state.celebrities = celebrities;
    }
  },
  actions: {
    async fetchCelebrities({ commit }) {
      try {
        const { data: celebrities } = await getCelebrities();
        const mappedCelebrities = celebrities.map(celebrity => {
          const { likePercentage, dislikePercentage } = getVotePercentages(
            celebrity
          );
          return {
            ...celebrity,
            likePercentage,
            dislikePercentage,
            hasSubmitted: false,
            hasLiked: null,
            hasErrors: false
          };
        });
        commit("SET_CELEBRITIES", mappedCelebrities);
      } catch (error) {
        console.error(error);
      }
    },
    handleVote({ state, commit }, celebrity) {
      const { celebrities } = state;
      const { vote } = celebrity;
      const votedUp = vote === "up";
      const updates = { hasLiked: votedUp };
      const updatedCelebrities = updateCelebrities(
        celebrities,
        celebrity,
        updates
      );
      commit("SET_CELEBRITIES", updatedCelebrities);
    },
    async handleVoteSubmission({ state, commit }, celebrity) {
      const { celebrities } = state;
      const { id, hasLiked } = celebrity;
      if (hasLiked === null) return;
      let updatedCelebrities;
      try {
        const vote = processVote(celebrity);
        const { data: patchedCelebrity } = await patchVote(id, vote);
        const { likePercentage, dislikePercentage } = getVotePercentages(
          patchedCelebrity
        );
        const updates = {
          likePercentage,
          dislikePercentage,
          hasSubmitted: true
        };
        updatedCelebrities = updateCelebrities(
          celebrities,
          patchedCelebrity,
          updates
        );
      } catch (error) {
        console.error(error);
        const updates = { hasSubmitted: true, hasErrors: true };
        updatedCelebrities = updateCelebrities(celebrities, celebrity, updates);
      } finally {
        commit("SET_CELEBRITIES", updatedCelebrities);
      }
    },
    handleVoteAgain({ state, commit }, celebrity) {
      const { celebrities } = state;
      const updates = { hasSubmitted: false, hasLiked: null, hasErrors: false };
      const updatedCelebrities = updateCelebrities(
        celebrities,
        celebrity,
        updates
      );
      commit("SET_CELEBRITIES", updatedCelebrities);
    }
  },
  getters: {
    celebrities(state) {
      return state.celebrities;
    }
  }
};
