import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("@/views/Home.vue")
  },
  {
    path: "/PastTrials",
    name: "PastTrials",
    component: () => import("@/views/PastTrials.vue")
  },
  {
    path: "/HowItWorks",
    name: "HowItWorks",
    component: () => import("@/views/HowItWorks.vue")
  },
  {
    path: "*",
    name: "PageNotFound",
    component: () => import("@/views/PageNotFound.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
