// Box
export const getVotePercentages = (source, limit = 100) => {
  const totalVotes = source.thumbsUp + source.thumbsDown;
  const likePercentage = ((source.thumbsUp / totalVotes) * limit).toFixed(1);
  const dislikePercentage = limit - likePercentage;
  return { likePercentage, dislikePercentage };
};
export const updateCelebrities = (celebrities, currentCelebrity, updates) => {
  const newCelebrities = [...celebrities];
  const currentIndex = newCelebrities.findIndex(
    celebrity => celebrity.id === currentCelebrity.id
  );
  const updatedCelebrity = { ...currentCelebrity, ...updates };
  newCelebrities.splice(currentIndex, 1, updatedCelebrity);
  return newCelebrities;
};
export const processVote = source =>
  source.hasLiked
    ? { thumbsUp: source.thumbsUp + 1 }
    : { thumbsDown: source.thumbsDown + 1 };
